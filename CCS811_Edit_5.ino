
#include "Adafruit_CCS811.h"
#include <dht.h>
dht DHT;
#define DHT11_PIN 8
Adafruit_CCS811 ccs;
int led1 = 9;
int led2 = 10;
int led3 = 11;
int led4 = 12;
int led5 = 13;

int status_led1 = 0;
int status_led2 = 0;
int status_led3 = 0;
int status_led4 = 0;
int status_led5 = 0;

int   status_co2=0;
float status_temperatura=0.0;
float status_humedad_Rel=0.0;
int   status_tvoc=0;

void setup() {
  pinMode(48, OUTPUT);         // Configuración Inicial
  pinMode(49, OUTPUT);
  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);
  pinMode(led3, OUTPUT);
  pinMode(led4, OUTPUT);
  pinMode(led5, OUTPUT);

  Serial.begin(9600);
  
  Serial.println("CCS811 test");

  if(!ccs.begin()){
    Serial.println("Failed to start sensor! Please check your wiring.");
    while(1);
  }
  while(!ccs.available());     // Esperar a que el Sensor este Listo para Medir
}

void loop() {
  
  DHT.read11(DHT11_PIN);
  
  if(ccs.available()){         // Lectura de Datos de Sensores y Comunicación con Puerto Serie 
    if(!ccs.readData()){
      // Lectura de datos
      status_co2 = ccs.geteCO2();
      status_temperatura = DHT.temperature;
      status_humedad_Rel = DHT.humidity;
      status_tvoc = ccs.getTVOC();

      // Comunicacion Serial
      // CO2 Y TVOC// 
      Serial.print("CO2= ");
      Serial.print(status_co2);
      Serial.println("ppm");
      Serial.print("TVOC= ");
      Serial.print(status_tvoc);
      Serial.println(" ppb");
      
      //TEMPERATURA//
      Serial.print("Temperatura = ");
      Serial.print(status_temperatura);
      Serial.println(" C");

      //HUMEDAD RELATIVA//
      Serial.print("Humedad = ");
      Serial.print(status_humedad_Rel);
      Serial.println(" %");
    }
    else{
      Serial.println("ERROR!");
      while(1);
    }
  }
  delay(2000);
  
  // Reseteo de status de los leds
  status_led1 = 0;
  status_led2 = 0;
  status_led3 = 0;
  status_led4 = 0;
  status_led5 = 0;

  // Seleccion de leds a encender segun el valor de los sensores
  if(status_co2 > 0 || status_tvoc > 0){
    status_led1 = 1;
  }
  if(status_co2 > 600 || status_tvoc > 330){
    status_led2 = 1;
  }
  if(status_co2 > 1000 || status_tvoc > 1000){
    status_led3 = 1;
  }
  if(status_co2 > 1500 || status_tvoc > 3333                ){
    status_led4 = 1;
  }
  if(status_co2 > 2500 || status_tvoc > 8332){
    status_led5 = 1;
  }
  // Encendido de leds
  if(status_led1 == 1){
    digitalWrite(led1, HIGH);
  }
  else{
    digitalWrite(led1, LOW);
  }
  if(status_led2 == 1){
    digitalWrite(led2, HIGH);
  }
  else{
    digitalWrite(led2, LOW);
  }
  if(status_led3 == 1){
    digitalWrite(led3, HIGH);
  }
  else{
    digitalWrite(led3, LOW);
  }
  if (status_led4 == 1){
    digitalWrite(led4, HIGH);
  }
  else{
    digitalWrite(led4, LOW);
  }
  if (status_led5 == 1){
    digitalWrite(led5, HIGH);
  }
  else{
    digitalWrite(led5, LOW);
  }

  if(status_co2 >= 500){    // ACTUADOR 1: Activar O Desactivar Actuador de CO2 segun VALOR UMBRAL DEFINIDO
     digitalWrite(48, HIGH);
     while(0);
  }
   else{
     digitalWrite(48, LOW);
     while(0);
    }

  if(status_tvoc >= 50){     // ACTUADOR 2: Activar O Desactivar Actuador de TVOC segun VALOR UMBRAL DEFINIDO
     digitalWrite(49, HIGH);
     while(0);
  }
   else{
     digitalWrite(49, LOW);
     while(0);
    }

  delay(500);
}
